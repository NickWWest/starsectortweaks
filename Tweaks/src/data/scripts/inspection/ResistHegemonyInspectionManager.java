package data.scripts.inspection;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.intel.inspection.HegemonyInspectionIntel;
import com.fs.starfarer.api.impl.campaign.intel.inspection.HegemonyInspectionManager;
import com.fs.starfarer.api.util.WeightedRandomPicker;

import java.util.Random;

public class ResistHegemonyInspectionManager extends HegemonyInspectionManager {

    public void createInspection(Integer fpOverride) {

        MarketAPI target = null;
        float max = 0f;
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
            if (market.isPlayerOwned()) {
                float curr = getAICoreUseValue(market);
                if (curr > max) {
                    target = market;
                    max = curr;
                }
            }
        }

        if (target != null && max > 0) {
            WeightedRandomPicker<MarketAPI> picker = new WeightedRandomPicker<MarketAPI>(random);
            for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) {
                if (market.getFactionId().equals(Factions.HEGEMONY)) {
                    if (market.getMemoryWithoutUpdate().getBoolean(MemFlags.MARKET_MILITARY)) {
                        picker.add(market, market.getSize());
                    }
                }
            }
            MarketAPI from = picker.pick();
            if (from == null) return;

            float fp = 50 + threshold * 0.5f;
            //fp = 500;
            if (fpOverride != null) {
                fp = fpOverride;
            }
            intel = new HegemonyInspectionIntel(from, target, fp);

            if(intel.getOrders() != HegemonyInspectionIntel.AntiInspectionOrders.RESIST) {
                // janky, but it's the only way based on execution flow since the intel emitted before we can update the orders member
                Global.getSector().getIntelManager().removeIntel(intel);
                intel.setOrders(HegemonyInspectionIntel.AntiInspectionOrders.RESIST);

                Global.getSector().getIntelManager().addIntel(intel);
            }

            if (intel.isDone()) {
                intel = null;
                return;
            }
        } else {
            return;
        }

        numAttempts++;
        suspicion = 0f;
        threshold *= 2f;
        if (threshold > MAX_THRESHOLD) {
            threshold = MAX_THRESHOLD;
        }
    }

}

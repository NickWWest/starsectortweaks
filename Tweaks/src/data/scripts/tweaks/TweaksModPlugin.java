package data.scripts.tweaks;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.impl.campaign.DebugFlags;
import com.fs.starfarer.api.impl.campaign.intel.inspection.HegemonyInspectionManager;

import data.scripts.inspection.ResistHegemonyInspectionManager;
import org.apache.log4j.Logger;


import java.util.ArrayList;


public final class TweaksModPlugin extends BaseModPlugin {
    private static final Logger log = Global.getLogger(TweaksModPlugin.class);

    @Override
    public void onGameLoad(boolean newGame) {
//        DebugFlags.HEGEMONY_INSPECTION_DEBUG = true;

        // remove the old script, see if we've already added the new one
        ResistHegemonyInspectionManager resist = null;
        for(EveryFrameScript everyFrameScript : new ArrayList<>(Global.getSector().getScripts())){
            if(everyFrameScript.getClass().equals(ResistHegemonyInspectionManager.class)){
                resist = (ResistHegemonyInspectionManager) everyFrameScript;
            } else if(everyFrameScript.getClass().getSimpleName().contains("HegemonyInspectionManager")){ // rip out other subclasses, there can be only one
                Global.getSector().removeScript(everyFrameScript);
            }
        }

        if (resist == null) {
            resist = new ResistHegemonyInspectionManager();
            Global.getSector().addScript(resist);
        }

        Global.getSector().getMemoryWithoutUpdate().set(HegemonyInspectionManager.KEY, resist);

        log.info("Now resisting AI inspections by default");
    }
}
